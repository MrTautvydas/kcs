__dirname = __dirname.charAt(0).toUpperCase() + __dirname.slice(1);

module.exports = {
    entry: './src/main/resources/static/index.js',
    output: {
        filename: 'bundle.js',
        path: __dirname + '/src/main/resources/static/dist'
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            {
                test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader: 'file-loader',
                query: {
                    outputPath: 'dist/',
                    publicPath: '../../../dist/' // That's the important part
                }
            },
            {
                test: /\.json$/,
                loader: "json-loader",
            },
        ]
    }
};