import com.kcs.baigiamasis.Application;
import com.kcs.baigiamasis.controller.OrderController;
import com.kcs.baigiamasis.entity.Orders;
import com.kcs.baigiamasis.entity.Wafers;
import com.kcs.baigiamasis.repository.OrderRepository;
import com.kcs.baigiamasis.repository.WaferRepository;
import org.aspectj.weaver.ast.Or;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Iterator;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class DatabaseTest {

    @Autowired
    private WaferRepository waferRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Test
    public void testSearchWafer() {

        // Delete
        Iterator iterator = waferRepository.findAll().iterator();
        Wafers waferDelete;
        while(iterator.hasNext()) {
            waferDelete = (Wafers) iterator.next();
            waferRepository.delete(waferDelete.getId());
        }

        // Insert
        Wafers wafer1 = new Wafers();
        wafer1.setBoule("a");
        wafer1.setCore("a");
        wafer1.setSubCore("a");
        wafer1.setWaferId(5);
        waferRepository.save(wafer1);

        // Find
        Iterator iterator2 = waferRepository.findAll().iterator();
        Wafers wafer2 = new Wafers();
        while(iterator2.hasNext()) {
            wafer2 = (Wafers) iterator2.next();
        }

        // Assert
        assertWafersEqual(wafer1, wafer2);

    }

    private void assertWafersEqual(Wafers wafer1, Wafers wafer2) {
        assertEquals(wafer1.getId(), wafer2.getId());
        assertEquals(wafer1.getBoule(), wafer2.getBoule());
        assertEquals(wafer1.getSubCore(), wafer2.getSubCore());
        assertEquals(wafer1.getWaferId(), wafer2. getWaferId());
    }

    @Test
    public void testSearchOrder() {

        // Delete
        Iterator iteratorDelete = orderRepository.findAll().iterator();
        while(iteratorDelete.hasNext()) {
            Orders orderDelete = (Orders) iteratorDelete.next();
            orderRepository.delete(orderDelete.getId());
        }

        // Insert
        Orders order1 = new Orders("OrderId", 5, "No options", true, "Comments", "done");
        orderRepository.save(order1);

        // Find
        Iterator<Orders> iteratorAssert = orderRepository.findAll().iterator();
        Orders order2 = new Orders();
        while(iteratorAssert.hasNext()) {
            order2 = iteratorAssert.next();
        }

        // Assert
        assertOrderEquals(order1, order2);

    }

    private void assertOrderEquals(Orders order1, Orders order2) {
        assertEquals(order1.getId(), order2.getId());
        assertEquals(order1.getOrderId(), order2.getOrderId());
        assertEquals(order1.getQuantity(), order2.getQuantity());
        assertEquals(order1.getEdgeGrinding(), order2.getEdgeGrinding());
        assertEquals(order1.getOtherOptions(), order2.getOtherOptions());
        assertEquals(order1.getComments(), order2.getComments());
        assertEquals(order1.getDone(), order2.getDone());
    }


}
