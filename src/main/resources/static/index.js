import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './app/Routes';


import css from './css/main.css'
import ReactBootstrapTable from './css/react-bootstrap-table.css'
import Bootstrap from './ext/bootstrap/bootstrap.min';


ReactDOM.render(
    <Routes/>,
    document.getElementById('root')
);