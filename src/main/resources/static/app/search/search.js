import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Service from '../shared/service';
import Table from '../shared/tables/table';
import Filters from '../shared/filters';


export default class Search extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            boule: "",
            core: "",
            subCore: "",
            waferId: "",
            orderId: "",
            uniqueWafer: [],
            callback: false,
            showWaferList: false,
            results: []
        };
    }

    handleChange(event) {
        this.setState({
            [event.target.name] : event.target.value,
            callback : true,
            uniqueWafer: []
        });
    }

    componentDidUpdate() {
        if(this.state.callback && (this.state.boule !== null && this.state.boule !== "" || (this.state.orderId !== null && this.state.orderId !== ""))) {
            this.setState({callback : false});
            const states = this.state;
            const self = this;
            const url = '/wafers/data?boule=' + Service.filterParameter(states.boule) + '&core=' + Service.filterParameter(states.core) + '&subCore=' + Service.filterParameter(states.subCore) + '&waferId=' + Service.numberFilter(states.waferId) + '&orderId=' + Service.filterParameter(states.orderId);
            axios({
                method: 'get',
                url: url,
            }).then(function (results) {
                const content = results.data;
                const uniqueWafers = content.filter((obj, index, self) =>
                    self.findIndex((t) => {
                        return t.boule === obj.boule && t.core === obj.core && t.waferId === obj.waferId && t.orderId === obj.orderId;
                    }) === index);
                self.setState({
                    uniqueWafer: uniqueWafers,
                });
            })
            .catch(function (error) {
                console.log(error);
            });
        }
    }

    componentWillMount() {

    }


    render() {
        const boule = this.state.boule;
        const core = this.state.core;
        const subCore = this.state.subCore;
        const waferId = this.state.waferId;
        const uniqueWafer = this.state.uniqueWafer;
        const orderId = this.state.orderId;
        const selectOptions = [
            ["boule", false, "TextFilter"],
            ["core", false, "TextFilter"],
            ["subCore", false, "TextFilter"],
            ["waferId", false, "TextFilter"]
        ];

        return (
            <div>
                <div className="row">
                    <div className="col-md-1 col-md-offset-3">
                        <input type="text" className="form-control" name="boule" placeholder="Boule" value={boule} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-1">
                        <input type="text" className="form-control" name="core" placeholder="Core" value={core} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-1">
                        <input type="text" className="form-control" name="subCore" placeholder="SubCore" value={subCore} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-1">
                        <input type="text" className="form-control" name="waferId" placeholder="WaferID" value={waferId} onChange={this.handleChange} />
                    </div>
                    <div className="col-md-2">
                        <input type="text" className="form-control" name="orderId" placeholder="Order name" value={orderId} onChange={this.handleChange} />
                    </div>
                </div>
                <Table data={uniqueWafer} deleteRow={true} selectOptions={selectOptions} />
            </div>
        )
    }
};