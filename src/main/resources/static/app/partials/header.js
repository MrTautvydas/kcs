import React from 'react';
import { Link } from 'react-router-dom';

export default class Header extends React.Component {

    render() {
        return(
            <div>
                <div className="row margin-bottom-2 margin-top-2">
                    <div className="col-md-8 col-md-offset-4">
                        <ul className="nav nav-pills text-center">
                            <li className={(location.pathname === "/search" || location.pathname === "/") ? "active" : ""}>
                                <Link to="/search">Search Wafers</Link></li>
                            <li className={(location.pathname === "/add") ? "active" : ""}>
                                <Link to="/add">Upload Wafers</Link>
                            </li>
                            <li className={(location.pathname === "/orders/all") ? "active" : ""}>
                                <Link to="/orders/all">Orders</Link>
                            </li>
                            <li className={(location.pathname === "/orders/new") ? "active" : ""}>
                                <Link to="/orders/new">New Order</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }

}