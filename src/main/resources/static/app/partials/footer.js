import React from 'react'

export default class Footer extends React.Component {
    render() {
        return (
            <div className="row padding-1 h5">
                <div className="col-md-12 text-center">
                    KCS - Finals &copy; {new Date().getFullYear()}
                </div>
            </div>
        )
    }
}