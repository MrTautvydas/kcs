import React from 'react';
import DropzoneComponent from 'react-dropzone-component';
import DropzoneCSS from '../../css/dropzone.css';
import Form from "./form";

export default class Insert extends React.Component {

    render(){
        window.getCookie = function(name) {
            const match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
            if (match) return match[2];
        };
        const componentConfig = {
            postUrl: '/wafers/upload',
        };
        const djsConfig = {
            headers: {'X-XSRF-TOKEN': window.getCookie('XSRF-TOKEN')},
            dictDefaultMessage: "Drag files or click on the field. File types accepted: .xlsx, .xls, .csv",
            dictInvalidFileType : "Invalid file type; Accepted: .xlsx, .xls, .csv",
            acceptedFiles: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, text/csv, application/vnd.ms-excel',
            accept: function(file, done) {
                const r = confirm("Are you sure you want to send file " + file.name + " ?");
                if (r === true) {
                    done();
                } else {
                    done("Failed to send file");
                }
            }
        };
        return(
            <div>
                <div className="row margin-bottom-2">
                    <div className="col-md-8 col-md-offset-2">
                        <div>
                            <DropzoneComponent className="h4" config={componentConfig}
                                               djsConfig={djsConfig} />
                        </div>
                    </div>
                </div>
                <p className="text-center">Or create a new wafer:</p>
                <div className="row margin-top-2">
                    <div className="col-md-2 col-md-offset-5">
                        <Form />
                    </div>
                </div>
            </div>

        )
    }

}