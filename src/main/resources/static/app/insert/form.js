import React from 'react';
import Service from '../shared/service';
import axios from 'axios';
import { withRouter } from 'react-router-dom';

export default class Form extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleBoule = this.handleBoule.bind(this);
        this.handleCore = this.handleCore.bind(this);
        this.handleSubCore = this.handleSubCore.bind(this);
        this.handleWaferId = this.handleWaferId.bind(this);
        this.state = {
            boule: "",
            core: "",
            subCore: "",
            waferId: "",
            bouleError: true,
            coreError: true,
            subCoreError: true,
            waferIdError: true,
            bouleRegex: "^[0-9A-Za-z_.-]+$",
            coreRegex: "^[0-9A-Za-z_.-]+$",
            subCoreRegex: "^[0-9A-Za-z_.-]+$",
            waferIdRegex: "^[0-9]+$"
        }
    }

    handleSubmit(event) {
        if(!Service.validate(this.state.bouleRegex, this.state.boule)) {
            alert("Boule is incorrect");
        } else if(!Service.validate(this.state.coreRegex, this.state.core)) {
            alert("Core is incorrect");
        } else if (!Service.validate(this.state.subCoreRegex, this.state.subCore)) {
            alert("subCore is incorrect");
        } else if (!Service.validate(this.state.waferIdRegex, this.state.waferId)) {
            alert("waferId is incorrect");
        } else {
            const self = this;
            axios.post('/wafers/add', {
                boule: this.state.boule,
                core: this.state.core,
                subCore: this.state.subCore,
                waferId: this.state.waferId,
                startTime: new Date().getTime()
            })
            .then(function (response) {
                alert("You have successfully added a wafer");
                console.log(response);
                self.setState({
                    boule: "",
                    core: "",
                    subCore: "",
                    waferId: ""
                })
            })
            .catch(function (error) {
                console.log(error);
            });
        }
        event.preventDefault();
    }

    handleBoule(event) {
        this.setState({
            boule: event.target.value,
            bouleError: Service.validate(this.state.bouleRegex, event.target.value)
        });
    }

    handleCore(event) {
        this.setState({
            core: event.target.value,
            coreError: Service.validate(this.state.coreRegex, event.target.value)
        });
    }

    handleSubCore(event) {
        this.setState({
            subCore: event.target.value,
            subCoreError: Service.validate(this.state.subCoreRegex, event.target.value)
        });
    }

    handleWaferId(event) {
        this.setState({
            waferId: event.target.value,
            waferIdError: Service.validate(this.state.waferIdRegex, event.target.value)
        });
    }

    componentDidUpdate() {
    }

    render() {
        return(
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className={this.state.bouleError ? "form-group" : "has-error form-group" }>
                        <label>Boule</label>
                        <input type="text" className="form-control" name="boule" value={this.state.boule} onChange={this.handleBoule} />
                    </div>
                    <div className={this.state.coreError ? "form-group" : "has-error form-group" }>
                        <label>Core</label>
                        <input type="text" className="form-control" name="core" value={this.state.core} onChange={this.handleCore} />
                    </div>
                    <div className={this.state.subCoreError ? "form-group" : "has-error form-group" }>
                        <label>SubCore</label>
                        <input type="text" className="form-control" name="subCore" value={this.state.subCore} onChange={this.handleSubCore} />
                    </div>
                    <div className={this.state.waferIdError ? "form-group" : "has-error form-group" }>
                        <label>WaferID</label>
                        <input type="text" className="form-control" name="waferId" value={this.state.waferId} onChange={this.handleWaferId} />
                    </div>
                    <button type="submit" className="btn btn-default margin-top-2">Submit</button>
                </form>
            </div>
        )
    }

}