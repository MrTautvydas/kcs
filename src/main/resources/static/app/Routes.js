import React from 'react';
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';
import Header from "./partials/header";
import Footer from "./partials/footer";
import Search from "./search/search";
import Insert from "./insert/insert";
import Order from "./order/order";
import OrderTable from "./order/ordertable";

const Routes = () => (
    <Router>
        <div className="container-fluid h4">
            <Header/>
            <Route exact path="/" component={Search}/>
            <Route path="/search" component={Search}/>
            <Route path="/add" component={Insert}/>
            <Route path="/orders/all" component={OrderTable}/>
            <Route path="/orders/new" component={Order}/>
            <Footer/>
        </div>
    </Router>
);

export default Routes