import React from 'react';
import axios from 'axios';
import { BootstrapTable, TableHeaderColumn, ButtonGroup } from 'react-bootstrap-table';



export default class OrderTable extends React.Component {
    constructor(props) {
        super(props);
        this.getOrderData = this.getOrderData.bind(this);
        this.createCustomButtonGroup = this.createCustomButtonGroup.bind(this);
        this.showAllOrders = this.showAllOrders.bind(this);
        this.state = {
            data: [],
            showAll: false,
        }
    }

    showAllOrders() {
        if(!this.state.showAll) {
            const self = this;
            axios.get('/orders/allorders')
                .then(function (results) {
                    self.setState({
                        data: results.data,
                        showAll: true
                    })
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else {
            this.getOrderData();
        }
    }

    getOrderData() {
        const self = this;
        axios.get('/orders/undoneorders')
            .then(function (results) {
                self.setState({
                    data: results.data,
                    showAll: false
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    componentWillMount() {
        this.getOrderData();
    }

    onAfterDeleteRow (rowKeys) {
        for(let i = 0; rowKeys.length > i; i++) {
            axios.delete('/orders/' + rowKeys[i])
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            })
        }
    }

    onAfterSaveCell (row) {
        axios.put('/orders', row)
            .then(function(response) {
                console.log(response);
            })
            .catch(function(error) {
                console.log(error);
            });

    }

    createCustomButtonGroup (props) {
        return (
            <ButtonGroup className='my-custom-class' sizeClass='btn-group-md'>
                { props.showSelectedOnlyBtn }
                { props.exportCSVBtn }
                { props.insertBtn }
                { props.deleteBtn }
                <button type='button'
                        className={ `btn btn-primary` }
                        onClick={this.showAllOrders}>
                    {this.state.showAll ? "Show uncompleted" : "Show all"}
                </button>
            </ButtonGroup>
        );
    };

    render() {
        const data = this.state.data;

        const options = {
            btnGroup: this.createCustomButtonGroup,
            afterDeleteRow: this.onAfterDeleteRow,
            exportCSVText: 'Export to .csv',
            deleteText: 'Delete',
            defaultSortName: "quantity",
            defaultSortOrder: "desc",
        };
        const selectRowProp = {
            mode: 'checkbox',
            bgColor: 'pink',
        };

        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell
        };

        const activeFormatter = (cell, row) => {
            return(
                <div>
                    {cell ? "Grinded" : "Sharp"}
                </div>
            )
        };


        return(
            <div>
                <BootstrapTable data={ data } options={options} selectRow={selectRowProp}
                                pagination={ true } exportCSV={ true }
                                csvFileName='orders.csv' hover deleteRow={ true } cellEdit={ cellEditProp } >
                    <TableHeaderColumn dataField='id' editable={false} isKey={ true } hidden={true}>id</TableHeaderColumn>
                    <TableHeaderColumn dataField='orderId' editable={false}>Order Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='quantity' editable={false} dataSort={true}>Quantity</TableHeaderColumn>
                    <TableHeaderColumn dataField='edgeGrinding' editable={false}  dataFormat={ activeFormatter }>Edges</TableHeaderColumn>
                    <TableHeaderColumn dataField='otherOptions' editable={false}>Order Options</TableHeaderColumn>
                    <TableHeaderColumn dataField='comments' editable={ { type: 'textarea' } }>Comments</TableHeaderColumn>
                    <TableHeaderColumn dataField='done' editable={ { type: 'select', options: {values: ["Completed", "In progress"] }} } >Order Status</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }

}