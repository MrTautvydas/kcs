import React from 'react';
import axios from 'axios';
import Service from '../shared/service';


export default class Order extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            orderId: "",
            quantity: "",
            edgeGrinding: true,
            comments: "",
            done: false,
            orderIdError: true,
            quantityError: true,
            orderRegex: "^[0-9A-Za-z-_.]+$",
            quantityRegex: "^[0-9]+$",

            selectedOption: "Edge grinding",
            otherOptions: "",

            cutWafers: 0,

        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleQuantity = this.handleQuantity.bind(this);
        this.handleOrderId = this.handleOrderId.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
        this.handleCommentsChange = this.handleCommentsChange.bind(this);
    }


    componentWillMount() {
        const self = this;
        axios.get('/wafers/cut')
            .then(function(results) {
                self.setState({
                    cutWafers: results.data.length
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    handleCommentsChange(event) {
        this.setState({
            comments: event.target.value
        });
    }

    handleCheckBoxChange(event) {
        if(!this.state.otherOptions.includes(event.target.value)) {
            this.setState({
                otherOptions: this.state.otherOptions + event.target.value
            })
        } else {
            const value = this.state.otherOptions.replace(event.target.value, '');
            this.setState({
                otherOptions: value
            })
        }
    }

    handleRadioChange(event) {
        this.setState({
            selectedOption: event.target.value
        })
    }

    handleOrderId(event) {
        this.setState({
            orderId: event.target.value,
            orderIdError: Service.validate(this.state.orderRegex, event.target.value)
        })
    }

    handleQuantity(event) {
        this.setState({
            quantity: event.target.value,
            quantityError: Service.validate(this.state.quantityRegex, event.target.value)
        })
    }

    handleSubmit(event) {
        if(!Service.validate(this.state.orderRegex, this.state.orderId)) {
            alert("Order name is incorrect");
        } else if(!Service.validate(this.state.quantityRegex, this.state.quantity)) {
            alert("Quantity is incorrect");
        } else if(this.state.quantity > this.state.cutWafers) {
            alert("There are less wafers in the storage");
        } else {
            const self = this;
            axios.post('/orders', {
                orderId: this.state.orderId,
                quantity: this.state.quantity,
                edgeGrinding: this.state.selectedOption === "Edge grinding",
                otherOptions: this.state.otherOptions,
                comments: this.state.comments,
                done: "In progress"
            })
                .then(function(response){
                    alert(response.data);
                    let wafers = self.state.cutWafers;
                    if(response.data === "New order successfully registered") {
                        wafers = self.state.cutWafers - self.state.quantity;
                    }
                    console.log(response);
                    self.setState({
                        cutWafers: wafers,
                        orderId: "",
                        quantity: "",
                        comments: ""
                    })
                })
                .catch(function (error) {
                    console.log(error);
                    alert("Problem occurred while saving");
                });
        }

        event.preventDefault();
    }

    render() {
        return(
            <div>
                <div className="row margin-top-3">
                    <div className="col-md-2 col-md-offset-5">
                        <form onSubmit={this.handleSubmit}>
                            <div className={this.state.orderIdError ? "form-group" : "has-error form-group" }>
                                <label>Order name:</label>
                                <input type="text" className="form-control" name="orderId" value={this.state.orderId} onChange={this.handleOrderId} />
                            </div>
                            <div className={this.state.quantityError ? "form-group" : "has-error form-group" }>
                                <label>Wafer quantity (Total: { this.state.cutWafers }):</label>
                                <input type="text" className="form-control" name="quantity" value={this.state.quantity} onChange={this.handleQuantity} />
                            </div>
                            <div>
                                <label>Edges:</label>
                            </div>
                            <div className="form-group">
                                <div className="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios1" value="Edge grinding"
                                               checked={this.state.selectedOption === "Edge grinding"} onChange={this.handleRadioChange} />
                                        Grinded (round corners)
                                    </label>
                                </div>
                                <div className="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" id="optionsRadios2" value="Sharp corners"
                                               checked={this.state.selectedOption === "Sharp corners"} onChange={this.handleRadioChange} />
                                        Sharp corners
                                    </label>
                                </div>
                            </div>
                            <div>
                                <label>Options:</label>
                            </div>
                            <div className="form-group">
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" value="Siconnex washing; " onChange={this.handleCheckBoxChange} />
                                        Siconnex wash
                                    </label>
                                </div>
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" value="Plasma eching; " onChange={this.handleCheckBoxChange} />
                                        Plasma eching
                                    </label>
                                </div>
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" value="Ultrasound after major steps; " onChange={this.handleCheckBoxChange} />
                                        Ultrasound after major steps
                                    </label>
                                </div>
                                <div className="checkbox">
                                    <label>
                                        <input type="checkbox" value="Thermo annealing; " onChange={this.handleCheckBoxChange} />
                                        Thermo annealed
                                    </label>
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Comments:</label>
                                <textarea className="form-control" rows={3} name="comments" value={this.state.comments} onChange={this.handleCommentsChange} />
                            </div>
                            <button type="submit" className="btn btn-default margin-top-1">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }

}