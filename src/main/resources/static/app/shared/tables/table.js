import React from 'react';
import axios from 'axios';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { ButtonGroup, Button, OverlayTrigger, Popover, Checkbox } from 'react-bootstrap';
import moment from 'moment';
import Service from '../../shared/service';


export default class Table extends React.Component {
    constructor(props) {
        super(props);
        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.state = {
            selectOptions: props.selectOptions,
            popover: "",
            columnSelect: localStorage.getItem("columnSelect")
        };
    }

    handleSelectChange(event) {
        const columnSelect = this.state.columnSelect;
        for(let i = 0; i < columnSelect.length; i++) {
            if(columnSelect[i][0] === event.target.value) {
                columnSelect[i] = [columnSelect[i][0], !columnSelect[i][1], columnSelect[i][2]];
                break;
            }
        }
        localStorage.setItem("columnSelect", JSON.stringify(columnSelect));
        this.setState({columnSelect: columnSelect});
    }

    componentWillMount() {
        let columnSelect = JSON.parse(this.state.columnSelect);
        if(typeof columnSelect === 'undefined' || columnSelect === null) {
            columnSelect = [
                ["id", true],
                ["boule", true, "TextFilter"],
                ["core", true, "TextFilter"],
                ["subCore", false, "TextFilter"],
                ["waferId", false, "TextFilter"],
                ["ttv", false, "NumberFilter"],
                ["warp", false, "NumberFilter"],
                ["bow", false, "NumberFilter"],
                ["thicknessAverage", false, "NumberFilter"],
                ["thicknessMax", true, "NumberFilter"],
                ["thicknessMin", false, "NumberFilter"],
                ["roughnessTop", false, "NumberFilter"],
                ["roughnessBottom", false, "NumberFilter"],
                ["wavinessTop", true, "NumberFilter"],
                ["wavinessBottom", true, "NumberFilter"],
                ["sawmarksTop", true, "NumberFilter"],
                ["sawmarksBottom", true, "NumberFilter"],
                ["soriTop", true, "NumberFilter"],
                ["soriBottom", true, "NumberFilter"],
                ["ltvMax", true, "NumberFilter"],
                ["ltvMin", true, "NumberFilter"],
                ["ltvDiff", true, "NumberFilter"],
                ["description", true, "TextFilter"],
                ["startTime", false, "TextFilter"],
                ["comments", false, "TextFilter"],
                ["orderId", false, "TextFilter"]
            ];
        }
        const selectOptions = this.state.selectOptions;
        if(typeof selectOptions !== 'undefined') {
            for(let i = 0; columnSelect.length > i; i++) {
                for(let r = 0; selectOptions.length > r; r++) {
                    if (columnSelect[i][0] === selectOptions[r][0]) {
                        columnSelect[i] = [selectOptions[r][0], selectOptions[r][1], selectOptions[r][2]];
                        break;
                    }
                }
            }
        }
        localStorage.setItem("columnSelect", JSON.stringify(columnSelect));
        this.setState({ columnSelect: columnSelect });
    }

    onAfterDeleteRow (rowKeys) {
        for(let i = 0; rowKeys.length > i; i++) {
            axios.delete('/wafers/' + rowKeys[i])
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                console.log(error);
            })
        }
    }

    onAfterSaveCell (row) {
        axios.put('/wafers', row)
            .then(function(response) {
                alert("Cell value was successfully modified");
                console.log(response);
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    onBeforeSaveCell(row, cellName, cellValue) {
        let columnSelect = JSON.parse(localStorage.getItem("columnSelect"));
        for(let i = 0; i < columnSelect.length; i++) {
            if(columnSelect[i][0] === cellName) {
                if(columnSelect[i][2] === "NumberFilter") {
                    return !!Service.validateWithValues("^([0-9-]+)?([.][0-9]+)?$", cellValue, cellName);
                } else if(columnSelect[i][2] === "TextFilter") {
                    if(columnSelect[i][0] === "boule" || columnSelect[i][0] === "core" || columnSelect[i][0] === "subCore") {
                        return !!Service.validateWithValues("^[0-9A-Za-z_-]+$", cellValue, cellName);
                    } else if(columnSelect[i][0] === "waferId") {
                        return !!Service.validateWithValues("^[0-9]+$", cellValue, cellName);
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        }
    }


    render() {
        const data = this.props.data;
        // SELECT COLUMN
        const columnSelect = this.state.columnSelect;
        const popover = (
            <Popover id="popover-trigger-click-root-close" className="column-filter-popover">
                {columnSelect.map(column =>
                    <Checkbox className={column[0] === "id" ? "hidden": ""} inline value={column[0]} key={column[0]} checked={!column[1]}
                              onChange={this.handleSelectChange}
                    >{column[0] } |</Checkbox>
                )}
            </Popover>
        );
        // DELETE ROW
        let deleteRow = this.props.deleteRow;
        if(typeof deleteRow === 'undefined') {
            deleteRow = false;
        }
        // TABLE OPTIONS
        const options = {
            afterDeleteRow: this.onAfterDeleteRow,
            exportCSVText: 'Export to .csv',
            deleteText: 'Delete',
            defaultSortName: "waferId",
            defaultSortOrder: "acs"
        };
        const selectRowProp = {
            mode: 'checkbox',
            bgColor: 'pink',
            // showOnlySelected: true, // <- rodyti tik pasirinktas eilutes
            // clickToSelect: true, // <- ar select'inti eilute paspaudus ne ant checkbox'o?
            // clickToSelectAndEditCell: true
        };
        const cellEditProp = {
            mode: 'click',
            blurToSave: true,
            afterSaveCell: this.onAfterSaveCell,
            beforeSaveCell: this.onBeforeSaveCell
        };

        const startTimeFormatter = (cell, row) => {
            return moment(cell).format("YYYY-MM-DD HH:mm");
        };

        const orderIdFormatter = (cell, row) => {
            if(row.orders !== null) {
                return row.orders.orderId;
            } else {
                return cell;
            }
        };

        const otherFormatter = (cell, row) => {
            return cell;
        };


        if (data !== 'undefined' && data !== "") {
            return (
                <div>
                    <div className="column-filter-btn">
                        <ButtonGroup>
                            <OverlayTrigger trigger="click" rootClose placement="bottom" overlay={popover}>
                                <Button>Column filter</Button>
                            </OverlayTrigger>
                        </ButtonGroup>
                    </div>
                    <BootstrapTable data={ data } options={options} selectRow={selectRowProp}
                                    keyField='id'  pagination={ data.length > 30} exportCSV={ true }
                                    csvFileName='export-data.csv' cellEdit={ cellEditProp } hover
                                    deleteRow={ deleteRow }>
                        {columnSelect.map(column =>
                            <TableHeaderColumn key={column[0]} dataField={column[0]} dataSort={true}
                                               hidden={column[1]} editable={ column[0] !== "startTime" && column[0] !== "orderId" && column[0] !== "subCore" }
                                               dataFormat = { column[0] === "startTime" ? startTimeFormatter : column[0] === "orderId" ? orderIdFormatter : otherFormatter }
                            >{column[0] === "orderId" ? "Order Name" : column[0]}</TableHeaderColumn>
                        )}
                    </BootstrapTable>

                </div>
            )
        } else {
            return <div>Loading...</div>
        }
    }

}