import React from 'react';

export default ({

    latest: function(data) {
        return data.filter((obj, index, self) => self.findIndex((t) => {return t.waferId === obj.waferId; }) === index);
    },
    uniqueSubCores: function(data) {
        return data.filter((obj, index, self) => self.findIndex((t) => {return t.subCore === obj.subCore; }) === index);
    },
    fromSubCores: function(data, filter) {
        return data.filter(function(callback) { return callback.subCore === filter });
    }


})