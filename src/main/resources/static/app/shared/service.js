import React from 'react';

export default ({

    filterParameter: function(param) {
        if(typeof param !== "undefined") {
            return param.replace(/[\[\]`~+.,!@#$%^&*();\\/|<>"'{}=]/g, "").replace(/[\s-]/g, " ").replace(/__+/g, "_").replace(/^_/g, "");
        }
    },

    numberFilter: function (param) {
        if(typeof param !== "undefined") {
            if(!param.match("^[0-9]+$")) {
                return "";
            }
            return param;
        }
    },

    validate: function(regex, value) {
        return !!value.match(regex);
    },

    validateWithValues: function(regex, value, cellName) {
        if(value.match(regex)) {
            return true;
        } else {
            alert("The value " + value + " is not correct in the cell named " + cellName);
            return false;
        }
    }

});