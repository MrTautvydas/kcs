package com.kcs.baigiamasis.loader;

import com.kcs.baigiamasis.entity.Wafers;
import com.kcs.baigiamasis.repository.WaferRepository;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NonUniqueResultException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

public class UploadLoader {

    private String[] headers = {
            "Boule", "Core", "SubCore", "WaferID", "TTV", "ThicknessAverage",
            "ThicknessMax", "ThicknessMin", "SawmarksTop", "SawmarksBottom",
            "WavinessTop", "WavinessBottom", "RoughnessTop", "RoughnessBottom",
            "SORITop", "SORIBottom", "Warp", "Bow", "LTVMax",
            "LTVMin", "LTVDiff", "Description", "StartTime", "Bow2", "PointThickness1",
            "PointThickness2", "PointThickness3", "PointThickness4", "PointThickness5",
            "TTVPoint", "PointThicknessMax", "PointThicknessMin", "PointThicknessAverage",
            "SAGTop", "SAGBottom", "TIR", "GridSize", "Type", "Size", "TableGridSize"
    };

    private MultipartFile multipartFile;
    private Wafers wafers;


    public UploadLoader(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public void scanAndAdd(WaferRepository waferRepository) throws IOException {

        HashMap<Integer, String> headerMap = new HashMap<>();

        InputStream stream = new BufferedInputStream(this.multipartFile.getInputStream());
        XSSFWorkbook book = new XSSFWorkbook(stream);
        XSSFSheet sheet = book.getSheetAt(0);

        // Ieskom kiekvienoje eiluteje
        for(Row row : sheet) {
            if (headerMap.isEmpty()) {
                // Ieskom kiekviename eilutes langelyje
                for (int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
                    Cell cell = row.getCell(i);
                    if (cell.getCellTypeEnum() == CellType.STRING) {
                        String cellValue = cell.getStringCellValue().trim();
                        // Patikrinam ar cellValue reiksme yra headers masyve
                        if (Arrays.asList(headers).contains(cellValue)) {
                            // Jeigu atitinka, tuomet nuo tos vietos irasome kiekviena header, kartu su jo celes vieta, i hashmap.
                            headerMap.put(i, cellValue);
                        }
                    }
                }
            } else {
                wafers = new Wafers();
                for(int i = row.getFirstCellNum(); i < row.getLastCellNum(); i++) {
                    if(Arrays.asList(headers).contains(headerMap.get(i))) {
                        Cell cellValue = row.getCell(i);
                        if(cellValue != null) {
                            switch (cellValue.getCellTypeEnum()) {
                                case STRING:
                                    setValues(headerMap.get(i), cellValue.getStringCellValue().trim());
                                    break;
                                case NUMERIC:
                                    setValues(headerMap.get(i), String.valueOf(new BigDecimal(cellValue.getNumericCellValue())));
                                    break;
                                default:
                                    setValues(headerMap.get(i), "");
                                    break;
                            }
                        }
                    }
                }
                try {
                    if(wafers.getCore() == null) {
                        wafers.setCore("");
                    }
                    if(wafers.getSubCore() == null) {
                        wafers.setSubCore("");
                    }
                    UniqueLoader uniqueLoader = new UniqueLoader(wafers, waferRepository);
                    uniqueLoader.addUniqueWafers();
                } catch (DataIntegrityViolationException exception) {
                    System.out.println("Entry already exists with this date");
                }
            } //headermap Empty?
        } // eilute

    }

    private void setValues(String columnName, String stringValue) {

        if(Objects.equals(columnName, "Boule")) {
            wafers.setBoule(stringValue);
        } else if(Objects.equals(columnName, "Core")) {
            wafers.setCore(stringValue);
        } else if(Objects.equals(columnName, "SubCore")) {
            wafers.setSubCore(stringValue);
        } else if(Objects.equals(columnName, "WaferID")) {
            Integer value = null;
            try {
                value = Integer.valueOf(stringValue);
            } catch (NumberFormatException nfe) {
                System.out.println("WaferID exception => " + nfe.getMessage());
            }
            wafers.setWaferId(value);
        } else if(Objects.equals(columnName, "TTV")) {
            wafers.setTtv(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "ThicknessAverage")) {
            wafers.setThicknessAverage(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "ThicknessMax")) {
            wafers.setThicknessMax(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "ThicknessMin")) {
            wafers.setThicknessMin(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "SawmarksTop")) {
            wafers.setSawmarksTop(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "SawmarksBottom")) {
            wafers.setSawmarksBottom(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "WavinessTop")) {
            wafers.setWavinessTop(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "WavinessBottom")) {
            wafers.setWavinessBottom(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "RoughnessTop")) {
            wafers.setRoughnessTop(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "RoughnessBottom")) {
            wafers.setRoughnessBottom(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "SORITop")) {
            wafers.setSoriTop(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "SORIBottom")) {
            wafers.setSoriBottom(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "Warp")) {
            wafers.setWarp(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "Bow")) {
            wafers.setBow(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "LTVMax")) {
            wafers.setLtvMax(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "LTVMin")) {
            wafers.setLtvMin(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "LTVDiff")) {
            wafers.setLtvDiff(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "Description")) {
            wafers.setDescription(stringValue);
        } else if(Objects.equals(columnName, "StartTime")) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                Date parsedDate = dateFormat.parse(stringValue);
                Timestamp timestamp = new Timestamp(parsedDate.getTime());
                wafers.setStartTime(timestamp);
            } catch (DateTimeParseException e) {
                System.out.println("Failed to convert StartTime to Decimal => " + e.getMessage());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if(Objects.equals(columnName, "Bow2")) {
            wafers.setBow2(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThickness1")) {
            wafers.setPointThickness1(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThickness2")) {
            wafers.setPointThickness2(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThickness3")) {
            wafers.setPointThickness3(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThickness4")) {
            wafers.setPointThickness4(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThickness5")) {
            wafers.setPointThickness5(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "TTVPoint")) {
            wafers.setTtvPoint(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThicknessMax")) {
            wafers.setPointThicknessMax(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThicknessMin")) {
            wafers.setPointThicknessMin(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "PointThicknessAverage")) {
            wafers.setPointThicknessAverage(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "SAGTop")) {
            wafers.setSagTop(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "SAGBottom")) {
            wafers.setSagBottom(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "TIR")) {
            wafers.setTir(convertToDecimal(stringValue));
        } else if(Objects.equals(columnName, "GridSize")) {
            wafers.setGridSize(stringValue);
        } else if(Objects.equals(columnName, "Type")) {
            wafers.setType(stringValue);
        } else if(Objects.equals(columnName, "Size")) {
            wafers.setSize(stringValue);
        } else if(Objects.equals(columnName, "TableGridSize")) {
            wafers.setTableGridSize(stringValue);
        }

    }

    private BigDecimal convertToDecimal (String stringValue) {
        BigDecimal numericValue = null;
        try {
            numericValue = new BigDecimal(stringValue.replaceAll(",", "."));
        } catch (Exception e) {
            System.out.println("Failed to convert to Decimal => " + e.getMessage());
        }
        return numericValue;
    }

}
