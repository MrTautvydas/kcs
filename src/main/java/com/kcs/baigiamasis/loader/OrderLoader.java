package com.kcs.baigiamasis.loader;

import com.kcs.baigiamasis.entity.Orders;
import com.kcs.baigiamasis.entity.Wafers;
import com.kcs.baigiamasis.repository.OrderRepository;
import com.kcs.baigiamasis.repository.WaferRepository;

import java.util.Iterator;

public class OrderLoader {

    private Orders order;

    public OrderLoader(Orders order) {
        this.order = order;
    }

    public void assignOrderToWafer(WaferRepository waferRepository) {

        for (Wafers wafer : waferRepository.findAllAfterCut(order.getQuantity())) {
            wafer.setOrders(order);
            waferRepository.save(wafer);
        }

    }



}
