package com.kcs.baigiamasis.loader;

import com.kcs.baigiamasis.entity.Wafers;
import com.kcs.baigiamasis.repository.WaferRepository;

import javax.persistence.NonUniqueResultException;

public class UniqueLoader {

    private Wafers wafer;
    private WaferRepository waferRepository;

    public UniqueLoader(Wafers wafer, WaferRepository waferRepository) {
        this.wafer = wafer;
        this.waferRepository = waferRepository;
    }

    public void addUniqueWafers() {
        Wafers waferDuplicate = null;
        try {
            waferDuplicate = waferRepository.findDuplicate(wafer.getBoule(), wafer.getCore(), wafer.getSubCore(), wafer.getWaferId());
        } catch (NonUniqueResultException e) {
            System.out.println("Daugiau negu vienas elementas yra wafer Duplicate");
        }
        if(waferDuplicate == null) {
            waferRepository.save(wafer);
            System.out.println("Data added successfully");
        } else {
            if(wafer.getStartTime().getTime() > waferDuplicate.getStartTime().getTime()) {
                waferRepository.delete(waferDuplicate.getId());
                waferRepository.save(wafer);
                System.out.println("Duplicated data added successfully");
            } else {
                System.out.println("Wafer'io startTime yra zemesnis uz jau ideta");
            }
        }
    }

}
