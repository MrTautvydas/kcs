package com.kcs.baigiamasis.controller;

import com.kcs.baigiamasis.entity.Orders;
import com.kcs.baigiamasis.entity.Wafers;
import com.kcs.baigiamasis.loader.OrderLoader;
import com.kcs.baigiamasis.repository.OrderRepository;
import com.kcs.baigiamasis.repository.WaferRepository;
import com.mysql.jdbc.exceptions.MySQLDataException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RestController
@RequestMapping(value = {"/orders"})
public class OrderController {

    private final OrderRepository orderRepository;
    private final WaferRepository waferRepository;

    @Autowired
    public OrderController(OrderRepository orderRepository, WaferRepository waferRepository) {
        this.orderRepository = orderRepository;
        this.waferRepository = waferRepository;
    }
    
    @PostMapping
    public String addOrder(@RequestBody Orders order) {
        Orders order2 = new Orders();
        try {
            order2 = orderRepository.findByOrderId(order.getOrderId());
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }

        if(order2 == null) {
            orderRepository.save(order);
            OrderLoader orderLoader = new OrderLoader(order);
            orderLoader.assignOrderToWafer(waferRepository);
            return "New order successfully registered";
        } else {
            return "Order with this name is already registered";
        }
    }

    @DeleteMapping("/{id}")
    public void deleteOrder(@PathVariable Long id) {
        for(Wafers wafer: waferRepository.findByOrdersId(id)) {
            wafer.setOrders(null);
            waferRepository.save(wafer);
        }
        orderRepository.delete(id);
    }

    @GetMapping("/allorders")
    public Iterable<Orders> getAllOrders() {
        return orderRepository.findAll();
    }

    @GetMapping("/undoneorders")
    public Iterable<Orders> getUndoneOrders() {
        return orderRepository.findByDoneNotOrderByIdDesc("Completed");
    }

    @PutMapping
    public void updateOrder(@RequestBody Orders orders) {
        orderRepository.save(orders);
    }




}
