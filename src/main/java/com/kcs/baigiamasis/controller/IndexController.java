package com.kcs.baigiamasis.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping(value = {"/", "/search", "/add", "/orders/all", "/orders/new"})
    public String index() {
        return "index";
    }

}
