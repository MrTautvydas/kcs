package com.kcs.baigiamasis.controller;

import com.kcs.baigiamasis.entity.Orders;
import com.kcs.baigiamasis.entity.Wafers;
import com.kcs.baigiamasis.loader.UniqueLoader;
import com.kcs.baigiamasis.loader.UploadLoader;
import com.kcs.baigiamasis.repository.OrderRepository;
import com.kcs.baigiamasis.repository.WaferRepository;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.NonUniqueResultException;
import java.io.IOException;
import java.util.Iterator;

@RestController
@RequestMapping(value = {"/wafers"})
public class WaferController {

    private final WaferRepository waferRepository;

    private final OrderRepository orderRepository;

    @Autowired
    public WaferController(WaferRepository waferRepository, OrderRepository orderRepository) {
        this.waferRepository = waferRepository;
        this.orderRepository = orderRepository;
    }

    @GetMapping("/data")
    public Iterable<Wafers> getWafers(@RequestParam(value = "boule", required = false) String boule,
                                      @RequestParam(value = "core", required = false) String core,
                                      @RequestParam(value = "subCore", required = false) String subCore,
                                      @RequestParam(value = "waferId", required = false) String waferId,
                                      @RequestParam(value = "orderId", required = false) String orderId) {

        if(!orderId.isEmpty()) {
            return waferRepository.findByQuery(boule, core, subCore, waferId, orderId);
        }
//        return waferRepository.findByBouleContainingAndCoreContainingAndSubCoreContainingAndWaferIdLike(boule, core, subCore, Integer.valueOf(waferId), pageable);
        return waferRepository.findByQueryNoOrderId(boule, core, subCore, waferId);
    }

    @PostMapping("/upload")
    public void uploadWaferData(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        UploadLoader loader = new UploadLoader(multipartFile);
        loader.scanAndAdd(waferRepository);
    }

    @PostMapping("/add")
    public void addWafer(@RequestBody Wafers wafers) {
        UniqueLoader uniqueLoader = new UniqueLoader(wafers, waferRepository);
        uniqueLoader.addUniqueWafers();
    }

    @PutMapping
    public void updateWafer(@RequestBody Wafers wafers) {
        waferRepository.save(wafers);
    }

    @DeleteMapping("/{id}")
    public void deleteWafer(@PathVariable Long id) {
        Orders order = new Orders();
        try {
            order = orderRepository.findOne(waferRepository.findOne(id).getOrders().getId());
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        waferRepository.delete(id);

        if(order.getOrderId() != null) {
            Iterable<Wafers> iterator = waferRepository.findByOrdersId(order.getId());
            order.setQuantity((int) iterator.spliterator().getExactSizeIfKnown());
            orderRepository.save(order);
        }
    }

    @GetMapping("/cut")
    public Iterable<Wafers> getAllCutWafers() {
        return waferRepository.findAllAfterCut(999999999);
    }


}
