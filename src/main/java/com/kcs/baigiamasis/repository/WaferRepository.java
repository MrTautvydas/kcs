package com.kcs.baigiamasis.repository;

import com.kcs.baigiamasis.entity.Wafers;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

// CRUD reiskia Create, Read, Update, Delete
// PageAndSortingRepository extends CrudRepository

public interface WaferRepository extends CrudRepository<Wafers, Long> {

    @Query(value = "SELECT * FROM wafers WHERE boule LIKE CONCAT('%',:boule,'%') " +
            "AND core LIKE CONCAT('%',:core,'%') AND sub_core LIKE CONCAT('%',:subCore,'%') " +
            "AND (wafer_id IS NULL OR wafer_id LIKE CONCAT('%',:waferId,'%')) AND orders_id IN " +
            "(SELECT id FROM orders WHERE order_id LIKE :orderId)" +
            " ORDER BY wafer_id DESC"
            , nativeQuery = true)
    Iterable<Wafers> findByQuery(@Param("boule") String boule,
                                 @Param("core") String core,
                                 @Param("subCore") String subCore,
                                 @Param("waferId") String waferId,
                                 @Param("orderId") String orderId);


    @Query(value = "SELECT * FROM wafers WHERE orders_id IS NULL AND sub_core LIKE 'W' " +
            "AND id NOT IN (SELECT id FROM wafers WHERE orders_id IS NULL AND sub_core LIKE 'W' AND " +
            "boule IN (SELECT boule FROM wafers WHERE sub_core NOT LIKE 'W') " +
            "AND core IN (SELECT core FROM wafers WHERE sub_core NOT LIKE 'W') " +
            "AND wafer_id IN (SELECT wafer_id FROM wafers WHERE sub_core NOT LIKE 'W') " +
            "GROUP BY boule, core, wafer_id)" +
            "GROUP BY boule, core, wafer_id ORDER BY start_time LIMIT :quantity", nativeQuery = true)
    Iterable<Wafers> findAllAfterCut(@Param("quantity") Integer quantity);


    @Query(value = "SELECT * FROM wafers WHERE boule LIKE CONCAT('%',:boule,'%') " +
            "AND core LIKE CONCAT('%',:core,'%') " +
            "AND sub_core LIKE CONCAT('%',:subCore,'%') " +
            "AND (wafer_id IS NULL OR wafer_id LIKE CONCAT('%',:waferId,'%')) " +
            "ORDER BY wafer_id DESC LIMIT 30"
            , nativeQuery = true)
    Iterable<Wafers> findByQueryNoOrderId(@Param("boule") String boule,
                                          @Param("core") String core,
                                          @Param("subCore") String subCore,
                                          @Param("waferId") String waferId);


    @Query(value = "SELECT * FROM wafers WHERE boule LIKE :boule " +
            "AND core LIKE :core AND sub_core LIKE :subCore AND wafer_id LIKE :waferId",
            nativeQuery = true)
    Wafers findDuplicate(@Param("boule") String boule,
                         @Param("core") String core,
                         @Param("subCore") String subCore,
                         @Param("waferId") Integer waferId);

    Iterable<Wafers> findByOrdersId(Long id);
}
