package com.kcs.baigiamasis.repository;

import com.kcs.baigiamasis.entity.Orders;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Orders, Long> {

    Iterable<Orders> findByDoneNotOrderByIdDesc(String completed);

    Orders findByOrderId(String orderId);
}
