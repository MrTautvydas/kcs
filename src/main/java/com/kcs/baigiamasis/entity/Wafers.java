package com.kcs.baigiamasis.entity;

import org.aspectj.weaver.ast.Or;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


@Entity
public class Wafers {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String boule;
    private String core;
    private String subCore;
    private Integer waferId;
    private BigDecimal ttv;
    private BigDecimal thicknessAverage;
    private BigDecimal thicknessMax;
    private BigDecimal thicknessMin;
    private BigDecimal sawmarksTop;
    private BigDecimal sawmarksBottom;
    private BigDecimal wavinessTop;
    private BigDecimal wavinessBottom;
    private BigDecimal roughnessTop;
    private BigDecimal roughnessBottom;
    private BigDecimal soriTop;
    private BigDecimal soriBottom;
    private BigDecimal warp;
    private BigDecimal bow;
    private BigDecimal ltvMax;
    private BigDecimal ltvMin;
    private BigDecimal ltvDiff;
    private String description;

    @Column(unique = true)
    private Timestamp startTime;

    private String comments;
    private BigDecimal bow2;
    private BigDecimal pointThickness1;
    private BigDecimal pointThickness2;
    private BigDecimal pointThickness3;
    private BigDecimal pointThickness4;
    private BigDecimal pointThickness5;
    private BigDecimal ttvPoint;
    private BigDecimal pointThicknessMax;
    private BigDecimal pointThicknessMin;
    private BigDecimal pointThicknessAverage;
    private BigDecimal sagTop;
    private BigDecimal sagBottom;
    private BigDecimal tir;
    private String gridSize;
    private String type;
    private String size;
    private String tableGridSize;

    @ManyToOne
    private Orders orders;

    // CONSTRUCTORS
    public Wafers() {}

    // GET-SET
    public Orders getOrders() { return orders;}

    public void setOrders(Orders orders) { this.orders = orders; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBoule() {
        return boule;
    }

    public void setBoule(String boule) {
        this.boule = boule;
    }

    public String getCore() {
        return core;
    }

    public void setCore(String core) {
        this.core = core;
    }

    public String getSubCore() {
        return subCore;
    }

    public void setSubCore(String subCore) {
        this.subCore = subCore;
    }

    public Integer getWaferId() {
        return waferId;
    }

    public void setWaferId(Integer waferId) {
        this.waferId = waferId;
    }

    public BigDecimal getTtv() {
        return ttv;
    }

    public void setTtv(BigDecimal ttv) {
        this.ttv = ttv;
    }

    public BigDecimal getThicknessAverage() {
        return thicknessAverage;
    }

    public void setThicknessAverage(BigDecimal thicknessAverage) {
        this.thicknessAverage = thicknessAverage;
    }

    public BigDecimal getThicknessMax() {
        return thicknessMax;
    }

    public void setThicknessMax(BigDecimal thicknessMax) {
        this.thicknessMax = thicknessMax;
    }

    public BigDecimal getThicknessMin() {
        return thicknessMin;
    }

    public void setThicknessMin(BigDecimal thicknessMin) {
        this.thicknessMin = thicknessMin;
    }

    public BigDecimal getSawmarksTop() {
        return sawmarksTop;
    }

    public void setSawmarksTop(BigDecimal sawmarksTop) {
        this.sawmarksTop = sawmarksTop;
    }

    public BigDecimal getSawmarksBottom() {
        return sawmarksBottom;
    }

    public void setSawmarksBottom(BigDecimal sawmarksBottom) {
        this.sawmarksBottom = sawmarksBottom;
    }

    public BigDecimal getWavinessTop() {
        return wavinessTop;
    }

    public void setWavinessTop(BigDecimal wavinessTop) {
        this.wavinessTop = wavinessTop;
    }

    public BigDecimal getWavinessBottom() {
        return wavinessBottom;
    }

    public void setWavinessBottom(BigDecimal wavinessBottom) {
        this.wavinessBottom = wavinessBottom;
    }

    public BigDecimal getRoughnessTop() {
        return roughnessTop;
    }

    public void setRoughnessTop(BigDecimal roughnessTop) {
        this.roughnessTop = roughnessTop;
    }

    public BigDecimal getRoughnessBottom() {
        return roughnessBottom;
    }

    public void setRoughnessBottom(BigDecimal roughnessBottom) {
        this.roughnessBottom = roughnessBottom;
    }

    public BigDecimal getSoriTop() {
        return soriTop;
    }

    public void setSoriTop(BigDecimal soriTop) {
        this.soriTop = soriTop;
    }

    public BigDecimal getSoriBottom() {
        return soriBottom;
    }

    public void setSoriBottom(BigDecimal soriBottom) {
        this.soriBottom = soriBottom;
    }

    public BigDecimal getWarp() {
        return warp;
    }

    public void setWarp(BigDecimal warp) {
        this.warp = warp;
    }

    public BigDecimal getBow() {
        return bow;
    }

    public void setBow(BigDecimal bow) {
        this.bow = bow;
    }

    public BigDecimal getLtvMax() {
        return ltvMax;
    }

    public void setLtvMax(BigDecimal ltvMax) {
        this.ltvMax = ltvMax;
    }

    public BigDecimal getLtvMin() {
        return ltvMin;
    }

    public void setLtvMin(BigDecimal ltvMin) {
        this.ltvMin = ltvMin;
    }

    public BigDecimal getLtvDiff() {
        return ltvDiff;
    }

    public void setLtvDiff(BigDecimal ltvDiff) {
        this.ltvDiff = ltvDiff;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public BigDecimal getBow2() {
        return bow2;
    }

    public void setBow2(BigDecimal bow2) {
        this.bow2 = bow2;
    }

    public BigDecimal getPointThickness1() {
        return pointThickness1;
    }

    public void setPointThickness1(BigDecimal pointThickness1) {
        this.pointThickness1 = pointThickness1;
    }

    public BigDecimal getPointThickness2() {
        return pointThickness2;
    }

    public void setPointThickness2(BigDecimal pointThickness2) {
        this.pointThickness2 = pointThickness2;
    }

    public BigDecimal getPointThickness3() {
        return pointThickness3;
    }

    public void setPointThickness3(BigDecimal pointThickness3) {
        this.pointThickness3 = pointThickness3;
    }

    public BigDecimal getPointThickness4() {
        return pointThickness4;
    }

    public void setPointThickness4(BigDecimal pointThickness4) {
        this.pointThickness4 = pointThickness4;
    }

    public BigDecimal getPointThickness5() {
        return pointThickness5;
    }

    public void setPointThickness5(BigDecimal pointThickness5) {
        this.pointThickness5 = pointThickness5;
    }

    public BigDecimal getTtvPoint() {
        return ttvPoint;
    }

    public void setTtvPoint(BigDecimal ttvPoint) {
        this.ttvPoint = ttvPoint;
    }

    public BigDecimal getPointThicknessMax() {
        return pointThicknessMax;
    }

    public void setPointThicknessMax(BigDecimal pointThicknessMax) {
        this.pointThicknessMax = pointThicknessMax;
    }

    public BigDecimal getPointThicknessMin() {
        return pointThicknessMin;
    }

    public void setPointThicknessMin(BigDecimal pointThicknessMin) {
        this.pointThicknessMin = pointThicknessMin;
    }

    public BigDecimal getPointThicknessAverage() {
        return pointThicknessAverage;
    }

    public void setPointThicknessAverage(BigDecimal pointThicknessAverage) {
        this.pointThicknessAverage = pointThicknessAverage;
    }

    public BigDecimal getSagTop() {
        return sagTop;
    }

    public void setSagTop(BigDecimal sagTop) {
        this.sagTop = sagTop;
    }

    public BigDecimal getSagBottom() {
        return sagBottom;
    }

    public void setSagBottom(BigDecimal sagBottom) {
        this.sagBottom = sagBottom;
    }

    public BigDecimal getTir() {
        return tir;
    }

    public void setTir(BigDecimal tir) {
        this.tir = tir;
    }

    public String getGridSize() {
        return gridSize;
    }

    public void setGridSize(String gridSize) {
        this.gridSize = gridSize;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTableGridSize() {
        return tableGridSize;
    }

    public void setTableGridSize(String tableGridSize) {
        this.tableGridSize = tableGridSize;
    }
}
