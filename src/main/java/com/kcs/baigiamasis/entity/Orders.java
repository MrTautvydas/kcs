package com.kcs.baigiamasis.entity;

import javax.persistence.*;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private String orderId;

    private Integer quantity;
    private String otherOptions;
    private Boolean edgeGrinding;
    private String comments;
    private String done;


    //CONSTRUCTOR

    public Orders() {}

    public Orders(String orderId, Integer quantity, String otherOptions, Boolean edgeGrinding,
                  String comments, String done) {
        this.orderId = orderId;
        this.quantity = quantity;
        this.otherOptions = otherOptions;
        this.edgeGrinding = edgeGrinding;
        this.comments = comments;
        this.done = done;
    }

    //GET-SET

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getOtherOptions() {
        return otherOptions;
    }

    public void setOtherOptions(String otherOptions) {
        this.otherOptions = otherOptions;
    }

    public Boolean getEdgeGrinding() {
        return edgeGrinding;
    }

    public void setEdgeGrinding(Boolean edgeGrinding) {
        this.edgeGrinding = edgeGrinding;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

}
